export const ckeditorPlugins = [
    "EasyImage",
    "ImageUpload",
    "Table",
    "TableToolbar",
    "MediaEmbed",
    "BlockQuote",
    "insertTable"
];

export const social = [
    {
        name: "facebook",
        icon: "facebook"
    },
    {
        name: "twitter",
        icon: "twitter"
    },
   /*  {
        name: "weibo",
        icon: "weibo"
    }, */
    {
        name: "wordpress",
        icon: "wordpress"
    },
    {
        name: "tumblr",
        icon: "tumblr"
    },
    {
        name: "reddit",
        icon: "reddit"
    },
    {
        name: "email",
        icon: "envelope-o"
    }
];

export const baseDomain = 'https://kenh14.vn/';

export const facebookQuote = 'You only live once _ Son Nguyen';