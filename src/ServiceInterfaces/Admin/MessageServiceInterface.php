<?php

namespace App\ServiceInterfaces\Admin;

interface MessageServiceInterface 
{
    public function getAllMessages();
}