<?php

namespace App\ServiceInterfaces\Api;

interface TagsServiceInterface
{
    public function getAllTags();
    
}