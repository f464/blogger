<?php

namespace App\ServiceInterfaces\Api;

interface CategoryServiceInterface
{
  public function getAllCategories();
}
